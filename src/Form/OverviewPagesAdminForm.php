<?php

namespace Drupal\vb_tweaks\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for tweaks.
 */
class OverviewPagesAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'overview_pages_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_tweaks.overview_pages.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_tweaks.overview_pages.settings');

    $node_bundles = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($node_bundles as $node_bundle) {
      $default_entity = FALSE;

      if ($config->get('overview_page_' . $node_bundle->id())) {
        $default_entity = \Drupal::entityTypeManager()->getStorage('node')
          ->load($config->get('overview_page_' . $node_bundle->id())[0]['target_id']);
      }

      $form['overview_page_' . $node_bundle->id()] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('Overview page ') . $node_bundle->label(),
        '#target_type' => 'node',
        '#tags' => TRUE,
        '#size' => 30,
        '#maxlength' => 1024,
        '#default_value' => $default_entity ?? NULL,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('vb_tweaks.overview_pages.settings');

    foreach ($values as $key => $value) {
      if (substr($key, 0, 14 ) === 'overview_page_') {
        $config->set($key, $value);
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
