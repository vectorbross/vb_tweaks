<?php

namespace Drupal\vb_tweaks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a 'CookieBot Content' block.
 *
 * @Block(
 *  id = "cookiebot_content_block",
 *  admin_label = @Translation("CookieBot Content block"),
 * )
 */
class CookieBotContentBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['langcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CookieBot language code'),
      '#default_value' => isset($config['langcode']) ? $config['langcode'] : ''
    ];

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('CookieBot Javascript url'),
      '#default_value' => isset($config['url']) ? $config['url'] : ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $this->configuration['langcode'] = $values['langcode'];
    $this->configuration['url'] = $values['url'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['#attributes']['class'][] = 'cookiebot-content';

    if($this->configuration['langcode'] && $this->configuration['url']) {
      $build['content'] = [
        '#theme' => 'cookiebot_content',
        '#langcode' => $this->configuration['langcode'],
        '#url' => $this->configuration['url'],
      ];
    }

    return $build;
  }

}
